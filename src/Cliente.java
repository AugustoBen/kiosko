
public class Cliente {
	private  String nombre;
	private String apellido;
	private int efectivo;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
		public Cliente(String nombre, String apellido, int efectivo) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.efectivo = efectivo;
	}
		public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public int getEfectivo() {
		return efectivo;
	}
	public void setEfectivo(int efectivo) {
		this.efectivo = efectivo;
	}

}
