
public class Vendedor {
	private  String nombre;
	private String apellido;
	private int vuelto;
	
	public int getVuelto() {
		return vuelto;
	}
	public void setVuelto(int vuelto) {
		this.vuelto = vuelto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Vendedor(String nombre, String apellido, int vuelto) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.vuelto = vuelto;
	}
	

}
