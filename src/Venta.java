public class Venta {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Cliente C1 = new Cliente("Juan", "De los Palotes", 100);
		Vendedor V1 = new Vendedor("Pedro", "Garay", 100);
		Producto P1 = new Producto("Alfajor", 25, 1);

		C1.setEfectivo(30);
	
		if (C1.getEfectivo() > P1.getPrecio()) {
			System.out.println("Te vendo "+P1.getDescripcion());
			System.out.println("El vuelto es "+(C1.getEfectivo()-P1.getPrecio()));
		} else {
			System.out.println("No te alcanza");
		}
			
	}

}
